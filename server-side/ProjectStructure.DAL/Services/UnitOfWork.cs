﻿using System.Threading.Tasks;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Abstract;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.DAL.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectsContext _context;

        public UnitOfWork(ProjectsContext context)
        {
            _context = context;
        }
        
        public IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(_context);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        
        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
﻿using System;

namespace ConsoleClient.Common.Models.Team
{
    public class TeamCreateModel
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
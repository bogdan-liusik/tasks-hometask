﻿using Xunit;
using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Fakes;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.BLL.Tests.Tests
{
    public class TeamServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _contextOptions;

        private TeamService _teamService;
        private UserService _userService;

        public TeamServiceTests()
        {
            _contextOptions = new DbContextOptionsBuilder<ProjectsContext>()
                .UseInMemoryDatabase("TestProjectsDB_TeamService")
                .Options;
            
            _mapper = new Mapper(new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<TeamProfile>();
                configuration.AddProfile<UserProfile>();
            }));
        }

        [Fact]
        public async Task CreateTeam_WhenAddUsersToTeam_ThenCheckTeamMembers()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _teamService = new TeamService(context, _mapper);
            _userService = new UserService(context, _mapper);
            
            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };

            var createdTeam = await _teamService.CreateTeam(team);

            var member1 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };

            var member2 = new UserCreateDTO()
            {
                FirstName = "Yaroslav",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "yaroslavliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2006"),
                CreatedAt = DateTime.Now
            };

            var createdMember1 = await _userService.CreateUser(member1);
            var createdMember2 = await _userService.CreateUser(member2);

            var result1 = await _userService.GetUserById(createdMember1.Id);
            var result2 = await _userService.GetUserById(createdMember2.Id);
            
            Assert.Equal(createdTeam.Id, result1.TeamId);
            Assert.Equal(createdTeam.Id, result2.TeamId);
        }
    }
}
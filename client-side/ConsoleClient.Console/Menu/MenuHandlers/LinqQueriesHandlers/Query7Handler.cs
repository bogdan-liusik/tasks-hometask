﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers
{
    using System;
    public class Query7Handler : BaseHandler
    {
        public Query7Handler(LinqQueriesService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the following structures: \n" +
                               "Project\n" +
                               "The longest project task (by description)\n" +
                               "The shortest project task (by name)\n" +
                               "Total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3", ConsoleColor.Yellow);

                var results = await (Service as LinqQueriesService).Query7();

                foreach (var result in results)
                {
                    WriteSuccessLine($"Project ID: {result.ProjectModel.Id} | Project Name: {result.ProjectModel.Name}");
                    WriteSuccessLine($"The longest project task (by description): \n" +
                                     $"Task ID: {result?.LongestTaskModelByDescription.Id} | Task description length: {result?.LongestTaskModelByDescription.Description.Length}");
                    WriteSuccessLine($"The shortest project task (by name): \n" +
                                     $"Task ID: {result?.ShortestTaskModelByName.Id} | Task name length: {result?.ShortestTaskModelByName.Name.Length}");
                    WriteSuccessLine($"Total number of users in the project team: {result?.TeamMembersCountByCondition}\n");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Tasks
{
    using System;
    public class DeleteTaskHandler : BaseHandler
    {
        public DeleteTaskHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                WriteSuccessLine("Enter task id: ");
                int id = Int32.Parse(Console.ReadLine());

                await (Service as CrudService).DeleteEntityAsync(id, "tasks");
                
                WriteSuccessLine("Task successfully deleted!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
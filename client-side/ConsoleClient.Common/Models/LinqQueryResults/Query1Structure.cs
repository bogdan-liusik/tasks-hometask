﻿using ConsoleClient.Common.Models.Project;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query1Structure
    {
        public ProjectModel ProjectModel { get; set; }
        public int TasksCount { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleClient.BLL.Services
{
    public class HttpClientService : IDisposable
    {
        private readonly HttpClient _httpClient;

        public HttpClientService(string baseAddress)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri(baseAddress)
            };
        }
        
        public async Task<TEntity> FetchObjectAsync<TEntity>(string endpoint)
        {
            var response = await FetchData(endpoint);
            return JsonConvert.DeserializeObject<TEntity>(await response.Content.ReadAsStringAsync());
        }
        
        public async Task<List<TEntity>> FetchListObjectsAsync<TEntity>(string endpoint)
        {
            var response = await FetchData(endpoint);
            return JsonConvert.DeserializeObject<List<TEntity>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<TResult> PostAsync<TEntity, TResult>(TEntity entity, string endpoint)
        {
            var serializedEntity = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");
            
            var response = await _httpClient.PostAsync(endpoint, serializedEntity);
            
            await HandleResponse(response);
            
            return JsonConvert.DeserializeObject<TResult>(await response.Content.ReadAsStringAsync());
        }

        public async Task<TResult> PutAsync<TEntity, TResult>(TEntity entity, string endpoint)
        {
            var serializedEntity = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json");
            
            var response = await _httpClient.PutAsync(endpoint, serializedEntity);
            
            await HandleResponse(response);
            
            return JsonConvert.DeserializeObject<TResult>(await response.Content.ReadAsStringAsync());
        }

        public async Task DeleteAsync(int id, string endpoint)
        {
            var response = await _httpClient.DeleteAsync($"{endpoint}/{id}");
            
            await HandleResponse(response);
        }

        private async Task<HttpResponseMessage> FetchData(string endpoint)
        {
            var response = await _httpClient.GetAsync(endpoint);
            await HandleResponse(response);
            return response;
        }

        private async Task HandleResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }
            var message = await response.Content.ReadAsStringAsync();
            throw new Exception(response.StatusCode + ": " + message);
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}
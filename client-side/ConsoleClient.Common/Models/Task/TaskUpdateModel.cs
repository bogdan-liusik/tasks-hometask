﻿using ConsoleClient.Common.Models.Task.Enums;

namespace ConsoleClient.Common.Models.Task
{
    public class TaskUpdateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
    }
}
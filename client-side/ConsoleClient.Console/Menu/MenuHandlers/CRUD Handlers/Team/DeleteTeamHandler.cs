﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Team
{
    using System;
    public class DeleteTeamHandler : BaseHandler
    {
        public DeleteTeamHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                WriteSuccessLine("Enter team id: ");
                int id = Int32.Parse(Console.ReadLine());

                await (Service as CrudService).DeleteEntityAsync(id, "teams");
                
                WriteSuccessLine("Team successfully deleted!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
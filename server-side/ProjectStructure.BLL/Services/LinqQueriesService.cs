﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;
using CustomTask = ProjectStructure.DAL.Entities.Task;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.BLL.Services
{
    public class LinqQueriesService : BaseService
    {
        public LinqQueriesService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }
        
        public async Task<ICollection<Query1StructureDTO>> Query1(int userId)
        {
            if (!await IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }

            var projects = await _context
                .Projects
                .Include(p => p.Tasks)
                .ToListAsync();
            
            return await Task.WhenAll(projects
                .Where(p => p.AuthorId == userId)
                .Select(async project => await Task.Run(() => new Query1StructureDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    TasksCount = project.Tasks.Count
                }))
                .ToList());
        }

        public async Task<ICollection<TaskDTO>> Query2(int userId)
        {
            if (!await IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            var tasks = await _context.Tasks.ToListAsync();
            
            return await Task.WhenAll(tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .Select(async task => await Task.Run(() => _mapper.Map<TaskDTO>(task)))
                .ToList());
        }

        public async Task<ICollection<Query3StructureDTO>> Query3(int userId)
        {
            if (!await IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            var currentYear = DateTime.Now.Year;

            var tasks = await _context.Tasks.ToListAsync();
            
            return await Task.WhenAll(tasks
                .Where(t => t.PerformerId == userId && t.FinishedAt.Year == currentYear)
                .Select(async task => await Task.Run(() => new Query3StructureDTO()
                {
                    Id = task.Id,
                    Name = task.Name
                }))
                .ToList());
        }

        public async Task<ICollection<Query4StructureDTO>> Query4()
        {
            var currentYear = DateTime.Now.Year;

            var teams = await _context.Teams
                .Include(t => t.Members)
                .ToListAsync();
            
            return await Task.WhenAll(teams.Select(async team => await Task.Run(() => new Query4StructureDTO()
            {
                TeamId = team.Id,
                TeamName = team.Name,
                Members = team.Members
                    .Where(u => u.TeamId == team.Id)
                    .Where(u => u.Birthday.Year < currentYear - 10)
                    .OrderByDescending(u => u.CreatedAt)
                    .Select(u => _mapper.Map<UserDTO>(u))
                    .ToList()
            })).ToList());
        }

        public async Task<ICollection<Query5StructureDTO>> Query5()
        {
            var users = await _context.Users
                .Include(u => u.Tasks)
                .ToListAsync();

            return await Task.WhenAll(users
                .OrderBy(u => u.FirstName)
                .Select(async (user) => await Task.Run(() => new Query5StructureDTO()
                {
                    User = _mapper.Map<UserDTO>(user),
                    Tasks = user.Tasks
                        .OrderByDescending(t => t.Name)
                        .Select(t => _mapper.Map<TaskDTO>(t)).ToList()
                })));
        }

        public async Task<Query6StructureDTO> Query6(int userId)
        {
            if (!await IsExistUser(userId))
            {
                throw new NotFoundException(typeof(User).ToString(), userId);
            }
            
            var users = await _context.Users
                .Include(u => u.Tasks)
                .Include(u => u.Projects)
                .ToListAsync();

            return users
                .Where(u => u.Id == userId)
                .Select(user => new Query6StructureDTO()
                {
                    User = _mapper.Map<UserDTO>(user),
                    LastProject = _mapper.Map<ProjectDTO>(user
                        .Projects
                        .OrderBy(p => p.CreatedAt)
                        .LastOrDefault()),
                    NumberOfTasksInLastProject = 
                        user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault().Tasks.Count,
                    IncompleteOrCancelledCount = 
                        user.Tasks.Count(t => t.FinishedAt == null || t.State == TaskState.Canceled),
                    LongestTaskByDate = 
                        _mapper.Map<TaskDTO>(user.Tasks?.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault())
                })
                .FirstOrDefault();
        }

        public async Task<ICollection<Query7StructureDTO>> Query7()
        {
            var projects = await _context.Projects
                .Include(p => p.Tasks)
                .Include(p => p.Team)
                    .ThenInclude(t => t.Members)
                .ToListAsync();
                
            return await Task.WhenAll(projects.Select(async project => await Task.Run(() => new Query7StructureDTO()
            {
                Project = _mapper.Map<ProjectDTO>(project),
                LongestTaskByDescription =
                    _mapper.Map<TaskDTO>(project.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                ShortestTaskByName =
                    _mapper.Map<TaskDTO>(project.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                TeamMembersCountByCondition = project.Description.Length > 20 || project.Tasks.Count < 3
                    ? project.Team.Members.Count
                    : default
            })));
        }
        
        private async Task<bool> IsExistUser(int userId)
        {
            return await _context.Users.AnyAsync(u => u.Id == userId);
        }
    }
}
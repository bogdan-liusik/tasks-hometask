﻿using System.Collections.Generic;
using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.User;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query5Structure
    {
        public UserModel UserModel { get; set; }
        public ICollection<TaskModel> Tasks { get; set; }
    }
}
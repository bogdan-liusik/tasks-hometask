﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.Task.Enums;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Tasks
{
    using System;
    public class CreateTaskHandler : BaseHandler
    {
        public CreateTaskHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                var taskCreate = new TaskCreateModel();
                WriteColorLine("Enter task name: ", ConsoleColor.Yellow);
                taskCreate.Name = Console.ReadLine();
                WriteColorLine("Enter task description: ", ConsoleColor.Yellow);
                taskCreate.Description = Console.ReadLine();
                WriteColorLine("Enter project ID: ", ConsoleColor.Yellow);
                taskCreate.ProjectId = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter performer ID: ", ConsoleColor.Yellow);
                taskCreate.PerformerId = Int32.Parse(Console.ReadLine());
                taskCreate.State = TaskState.ToDo;

                var createdTask = await (Service as CrudService).CreateEntityAsync<TaskCreateModel, TaskModel>(taskCreate, "tasks");
                
                WriteSuccessLine($"Task successfully created with id {createdTask.Id}!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteColorLine(ex.Message, ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
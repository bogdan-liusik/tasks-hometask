﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.User;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User
{
    using System;
    public class UpdateUserHandler : BaseHandler
    {
        public UpdateUserHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                var userUpdate = new UserUpdateModel();
                WriteColorLine("Enter id of user which you want to update: ", ConsoleColor.Yellow);
                userUpdate.Id = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter new first name: ", ConsoleColor.Yellow);
                userUpdate.FirstName = Console.ReadLine();
                WriteColorLine("Enter new last name: ", ConsoleColor.Yellow);
                userUpdate.LastName = Console.ReadLine();
                WriteColorLine("Enter new email: ", ConsoleColor.Yellow);
                userUpdate.Email = Console.ReadLine();

                await (Service as CrudService).UpdateEntityAsync<UserUpdateModel, UserModel>(userUpdate, "users");
                
                WriteSuccessLine("User successfully updated!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
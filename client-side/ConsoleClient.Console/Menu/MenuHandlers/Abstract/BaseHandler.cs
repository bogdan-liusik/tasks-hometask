﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services.Abstract;

namespace ConsoleClient.Console._Menu.MenuHandlers.Abstract
{
    public abstract class BaseHandler
    {
        private protected readonly BaseService Service;
            
        public BaseHandler(BaseService service)
        {
            Service = service;
        }

        public abstract Task Handle();
    }
}
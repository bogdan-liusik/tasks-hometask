﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User;
using ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Project;
using ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Tasks;
using ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Team;
using ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers;

namespace ConsoleClient.Console._Menu
{
     using static MenuHelper;
        
     public class Menu
     {
         private bool _running = true;
         private readonly Dictionary<int, BaseHandler> _handlers;
         private readonly HttpClientService _httpClientService;
            
         public Menu(HttpClientService httpClientService)
         {
             var linqQueriesService = new LinqQueriesService(httpClientService);
             var crudService = new CrudService(httpClientService);
             _handlers = new Dictionary<int, BaseHandler>()
             {
                 {1, new CreateUserHandler(crudService)},
                 {2, new UpdateUserHandler(crudService)},
                 {3, new GetUserHandler(crudService)},
                 {4, new DeleteUserHandler(crudService)},
                 
                 {5, new CreateProjectHandler(crudService)},
                 {6, new UpdateProjectHandler(crudService)},
                 {7, new GetProjectHandler(crudService)},
                 {8, new DeleteProjectHandler(crudService)},
                 
                 {9, new CreateTaskHandler(crudService)},
                 {10, new UpdateTaskHandler(crudService)},
                 {11, new GetTaskHandler(crudService)},
                 {12, new DeleteTaskHandler(crudService)},
                 
                 {13, new CreateTeamHandler(crudService)},
                 {14, new UpdateTeamHandler(crudService)},
                 {15, new GetTeamHandler(crudService)},
                 {16, new DeleteTeamHandler(crudService)},
                 
                 {17, new Query1Handler(linqQueriesService)},
                 {18, new Query2Handler(linqQueriesService)},
                 {19, new Query3Handler(linqQueriesService)},
                 {20, new Query4Handler(linqQueriesService)},
                 {21, new Query5Handler(linqQueriesService)},
                 {22, new Query6Handler(linqQueriesService)},
                 {23, new Query7Handler(linqQueriesService)},
             };

             _httpClientService = httpClientService;
         }
         
         public async Task Start()
         {
             while (_running)
             {
                 RunMarkRandomTaskWithDelay(_httpClientService);
                 ShowCommands();
                 await ChooseCommand(_handlers);
             }
         }
         
         private static async Task RunMarkRandomTaskWithDelay(HttpClientService httpClientService)
         {
             var queries = new DelayedTaskService(httpClientService);
             var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
             WriteColorLine($"\nTask with id {markedTaskId} successfully updated!", ConsoleColor.Yellow);
         }

         private async Task ChooseCommand(Dictionary<int, BaseHandler> handlers)
         {
             try
             {
                 System.Console.Write("Enter your choice: ");
                 
                 if(Int32.TryParse(System.Console.ReadLine(), out var menuItem))
                 {
                     if (menuItem <= handlers.Count && menuItem > 0)
                     {
                         await handlers[menuItem].Handle();
                     }
                     else if(menuItem == 0)
                     {
                         _running = false;
                     }
                     else
                     {
                         throw new InvalidOperationException($"Item number must be in range 1 - {handlers.Count}! Try Again.");
                     }
                 }
                 else
                 {
                     throw new InvalidOperationException("Invalid character! Try Again.");
                 }
             }
             catch (InvalidOperationException ex)
             {
                 WriteErrorLine(ex.Message);
             }
    
             WriteColorLine("\n" + new string('─', 81) + "\n", ConsoleColor.DarkGray);
         }
     }
}
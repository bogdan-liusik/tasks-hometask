﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Team;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Team
{
    using System;
    public class GetTeamHandler : BaseHandler
    {
        public GetTeamHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                WriteSuccessLine("Enter task id: ");
                int id = Int32.Parse(Console.ReadLine());
                
                var team = await (Service as CrudService).GetEntityByIdAsync<TeamModel>("teams", id);
                
                WriteColorLine($"Team ID: {team.Id}", ConsoleColor.Yellow);
                WriteColorLine($"Team name: {team.Name}", ConsoleColor.Yellow); ;
                WriteColorLine($"Team created at: {team.CreatedAt}", ConsoleColor.Yellow);

                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteErrorLine(ex.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
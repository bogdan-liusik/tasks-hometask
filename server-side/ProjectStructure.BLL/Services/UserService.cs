﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetAllUsers()
        {
            var users = await GetAllEntitiesAsync<User>();
            
            return await Task.WhenAll(users
                .Select(async user => await Task.Run(() => _mapper.Map<UserDTO>(user)))
                .ToList());
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var userEntity = await GetEntityByIdAsync<User>(id);
            return _mapper.Map<UserDTO>(userEntity);
        }
        
        public async Task<ICollection<TaskDTO>> GetAllUnfinishedTasks(int id)
        {
            if (!await _context.Users.AnyAsync(u => u.Id == id))
            {
                throw new NotFoundException(typeof(User).ToString(), id);
            }
            
            var user = await _context
                .Users
                .Include(u => u.Tasks)
                .SingleOrDefaultAsync(u => u.Id == id);
            
            return await Task.WhenAll(user.Tasks
                .Where(task => task.PerformerId == id && (task.State is TaskState.InProgress or TaskState.ToDo || task.FinishedAt == null))
                .Select(async task => await Task.Run(() => _mapper.Map<TaskDTO>(task)))
                .ToList());
        }

        public async Task<UserDTO> CreateUser(UserCreateDTO userCreateDto)
        {
            var userEntity = _mapper.Map<User>(userCreateDto);
            var createdUser = await _context.Users.AddAsync(userEntity);
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDTO>(createdUser.Entity);
        }

        public async Task<UserDTO> UpdateUser(UserUpdateDTO userUpdateDto)
        {
            var userEntity = await GetEntityByIdAsync<User>(userUpdateDto.Id);
            
            userEntity.FirstName = userUpdateDto.FirstName;
            userEntity.LastName = userUpdateDto.LastName;
            userEntity.Email = userUpdateDto.Email;
            
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDTO>(userEntity);
        }

        public async Task DeleteUser(int id)
        {
            var userEntity = await GetEntityByIdAsync<User>(id);
            _context.Users.Remove(userEntity);
            await _context.SaveChangesAsync();
        }
    }
}
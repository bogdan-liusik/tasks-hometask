﻿using ConsoleClient.Common.Models.Project;

namespace ConsoleClient.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly HttpClientService _httpClientService;
        
        public BaseService(HttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }
    }
}
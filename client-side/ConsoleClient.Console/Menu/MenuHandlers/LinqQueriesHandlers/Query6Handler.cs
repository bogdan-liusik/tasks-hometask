﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers
{
    using System;
    public class Query6Handler : BaseHandler
    {
        public Query6Handler(LinqQueriesService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the following structure (pass user Id in parameters): \n" +
                               "User\n" +
                               "User's last project (by creation date)\n" +
                               "Total number of tasks under the last project\n" +
                               "Total number of incomplete or cancelled tasks for the user\n" +
                               "Longest user task by date (earliest created - latest finished)", ConsoleColor.Yellow);

                Console.WriteLine("Enter user id: ");
                string id = Console.ReadLine();
                
                var result = await (Service as LinqQueriesService).Query6(Int32.Parse(id));

                if (result == null)
                {
                    WriteColorLine("Empty structure for this user.", ConsoleColor.Cyan);
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                WriteColorLine("Result: ", ConsoleColor.Yellow);
                WriteSuccessLine($"User ID: {result.UserModel.Id} | First Name: {result.UserModel.FirstName}");
                WriteSuccessLine($"User's last project (by creation date): \n" +
                                 $"ProjectID: {result.LastProjectModel.Id} | Name: {result.LastProjectModel.Name}");
                WriteSuccessLine($"Total number of tasks under the last project: {result.NumberOfTasksInLastProject}");
                WriteSuccessLine($"Total number of incomplete or cancelled tasks for the user: {result.IncompleteOrCancelledCount}");
                WriteSuccessLine($"Longest user task by date (earliest created - latest finished): \n" +
                                 $"Task ID: {result.LongestTaskModelByDate.Id} | Name: {result.LongestTaskModelByDate.Name}");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error... :(");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
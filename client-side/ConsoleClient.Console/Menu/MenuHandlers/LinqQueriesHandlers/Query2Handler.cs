﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers
{
    using System;
    public class Query2Handler : BaseHandler
    {
        public Query2Handler(LinqQueriesService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list of tasks assigned to a specific user (by id), where name of task < 45 characters (collection of tasks).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id: ");
                string id = Console.ReadLine();
                
                var result = await (Service as LinqQueriesService).Query2(Int32.Parse(id));

                if (result.Count == 0)
                {
                    Console.WriteLine("This user has no tasks.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var task in result)
                {
                    WriteSuccessLine($"TaskID: {task.Id} | Name: {task.Name} | Name Length: {task.Name.Length}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
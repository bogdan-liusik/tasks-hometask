﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Fakes;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using Xunit;
using Xunit.Abstractions;

namespace ProjectStructure.BLL.Tests.Tests
{
    public class LinqQueriesServiceTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _contextOptions;

        private TeamService _teamService;
        private TaskService _taskService;
        private LinqQueriesService _linqService;
        private ProjectService _projectService;
        private UserService _userService;

        public LinqQueriesServiceTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _contextOptions = new DbContextOptionsBuilder<ProjectsContext>()
                .UseInMemoryDatabase("TestProjectsDB_LINQ")
                .Options;

            var mapperConfiguration = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<UserProfile>();
                configuration.AddProfile<TaskProfile>();
                configuration.AddProfile<ProjectProfile>();
                configuration.AddProfile<TeamProfile>();
            });
            
            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public async Task Query1_WhenAddTwoTasks_ThenNumberOfTasksTwo()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _projectService = new ProjectService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            _teamService = new TeamService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };

            var createdUser = await _userService.CreateUser(user);

            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };

            var createdTeam = _teamService.CreateTeam(team);
            
            var project = new ProjectCreateDTO()
            {
                Name = "ProjectName",
                Description = "ProjectsDescription",
                AuthorId = createdUser.Id,
                TeamId = createdTeam.Id,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(5)
            };
            
            var createdProject = await _projectService.CreateProject(project);
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = 1,
                CreatedAt = DateTime.Now,
            };
            
            await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);

            _linqService = new LinqQueriesService(context, _mapper);

            var queryResult = await _linqService.Query1(createdUser.Id);
            _testOutputHelper.WriteLine(queryResult.Count.ToString());
            Assert.Equal(2, queryResult.ToList()[0].TasksCount);
        }
        
        [Fact]
        public async Task Query1_WhenUserDoesNotExist_ThenNotFoundException()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _linqService = new LinqQueriesService(context, _mapper);
            await Assert.ThrowsAsync<NotFoundException>(() => _linqService.Query1(1));
        }

        [Fact]
        public async Task Query2_WhenAddTwoTasksForUser_ThenGetTasksWhereLengthMoreThan45()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUser = await _userService.CreateUser(user);
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now,
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = $"TaskName {new string('.', 45)}",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now,
            };

            await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);
            
            _linqService = new LinqQueriesService(context, _mapper);

            var queryResult = await _linqService.Query2(createdUser.Id);
            Assert.Equal(1, queryResult.Count);
        }
        
        [Fact]
        public async Task Query2_WhenUserDoesNotExist_ThenNotFoundException()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _linqService = new LinqQueriesService(context, _mapper);
            await Assert.ThrowsAsync<NotFoundException>(() => _linqService.Query2(1));
        }

        [Fact]
        public async Task Query3_WhenAddOneTaskFinishedIn2021_ThenGetOneTask()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUser = await _userService.CreateUser(user);
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = $"TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
            };

            var createdTask1 = await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);
            
            _linqService = new LinqQueriesService(context, _mapper);

            var queryResult = await _linqService.Query3(createdUser.Id);
            Assert.Equal(1, queryResult.Count);
            Assert.Equal(createdTask1.Id, queryResult.ToList()[0].Id);
            Assert.Equal(createdTask1.Name, queryResult.ToList()[0].Name);
        }
        
        [Fact]
        public async Task Query3_WhenUserDoesNotExist_ThenNotFoundException()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _linqService = new LinqQueriesService(context, _mapper);
            await Assert.ThrowsAsync<NotFoundException>(() => _linqService.Query3(1));
        }
            
        [Fact]
        public async Task Query4_WhenAdd3UserOlderThan10Years_ThenGetTeamWithThisUserSortedByDescending()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _teamService = new TeamService(context, _mapper);
            
            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };

            var createdTeam = await _teamService.CreateTeam(team);
            
            var user1 = new UserCreateDTO()
            {
                FirstName = "Yasoslav",
                LastName = "Liusik",
                Email = "yaroslavliusik@gmail.com",
                TeamId = createdTeam.Id,
                BirthDay = DateTime.Parse("31/12/2015"),
                CreatedAt = DateTime.Now,
            };
            
            var user2 = new UserCreateDTO()
            {
                FirstName = "Stas",
                LastName = "Liusik",
                Email = "stasliusik@gmail.com",
                TeamId = createdTeam.Id,
                BirthDay = DateTime.Parse("31/12/2003"),
                CreatedAt = DateTime.Now.AddDays(-4),
            };
            
            var user3 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                TeamId = createdTeam.Id,
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now.AddDays(-5),
            };
            
            var user4 = new UserCreateDTO()
            {
                FirstName = "Steve",
                LastName = "Liusik",
                Email = "steveliusik@gmail.com",
                TeamId = createdTeam.Id,
                BirthDay = DateTime.Parse("31/12/2004"),
                CreatedAt = DateTime.Now.AddDays(-3),
            };
            
            var createdUser1 = await _userService.CreateUser(user1);
            var createdUser2 = await _userService.CreateUser(user2);
            var createdUser3 = await _userService.CreateUser(user3);
            var createdUser4 = await _userService.CreateUser(user4);

            _linqService = new LinqQueriesService(context, _mapper);
            var queryResult = await _linqService.Query4();

            Assert.Contains(queryResult, t => t.TeamId == createdTeam.Id);
            Assert.DoesNotContain(queryResult.ToList()[0].Members, u => u.Id == createdUser1.Id);
            
            //check descending sorting by CreatedAt.
            var sortedUsers = queryResult.ToList()[0].Members.ToList();
            
            Assert.Equal(createdUser4.Id, sortedUsers[0].Id);
            Assert.Equal(createdUser2.Id, sortedUsers[1].Id);
            Assert.Equal(createdUser3.Id, sortedUsers[2].Id);
        }
            
        [Fact]
        public async Task Query5_WhenAddTasksForUser_ThenCheckTaskSortingByNameForEveryUser()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            
            var user1 = new UserCreateDTO()
            {
                FirstName = "Yasoslav",
                LastName = "Liusik",
                Email = "yaroslavliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2015"),
                CreatedAt = DateTime.Now,
            };
            
            var user2 = new UserCreateDTO()
            {
                FirstName = "Stas",
                LastName = "Liusik",
                Email = "stasliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2003"),
                CreatedAt = DateTime.Now.AddDays(-4),
            };
            
            var user3 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now.AddDays(-5),
            };

            var createdUser1 = await _userService.CreateUser(user1);
            var createdUser2 = await _userService.CreateUser(user2);
            var createdUser3 = await _userService.CreateUser(user3);
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser1.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser1.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
            };
            
            var task3 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser2.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task4 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser2.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
            };
            
            var task5 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser3.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task6 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription", 
                ProjectId = 1,
                PerformerId = createdUser3.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
            };

            var createdTask1 = await _taskService.CreateTask(task1);
            var createdTask2 = await _taskService.CreateTask(task2);
            var createdTask3 = await _taskService.CreateTask(task3);
            var createdTask4 = await _taskService.CreateTask(task4);
            var createdTask5 = await _taskService.CreateTask(task5);
            var createdTask6 = await _taskService.CreateTask(task6);
            
            _linqService = new LinqQueriesService(context, _mapper);
            
            var queryResult = await _linqService.Query5();
            
            Assert.Equal(createdUser3.Id, queryResult.ToList()[0].User.Id);
            Assert.Equal(createdUser2.Id, queryResult.ToList()[1].User.Id);
            Assert.Equal(createdUser1.Id, queryResult.ToList()[2].User.Id );

            var sortedTasksForUser1 = queryResult.ToList()[0].Tasks.ToList();
            var sortedTasksForUser2 = queryResult.ToList()[1].Tasks.ToList();
            var sortedTasksForUser3 = queryResult.ToList()[2].Tasks.ToList();
            
            Assert.Equal(createdTask6.Id, sortedTasksForUser1[0].Id);
            Assert.Equal(createdTask5.Id, sortedTasksForUser1[1].Id);
            Assert.Equal(createdTask4.Id, sortedTasksForUser2[0].Id);
            Assert.Equal(createdTask3.Id, sortedTasksForUser2[1].Id);
            Assert.Equal(createdTask2.Id, sortedTasksForUser3[0].Id);
            Assert.Equal(createdTask1.Id, sortedTasksForUser3[1].Id);
        }
        
        [Fact]
        public async Task Query6_WhenUserDoesNotExist_ThenNotFoundException()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _linqService = new LinqQueriesService(context, _mapper);
            await Assert.ThrowsAsync<NotFoundException>(() => _linqService.Query6(1));
        }

        [Fact]
        public async Task Query6_GetExpectedStructure()
        {
            await using var context = new ProjectsContextFake(_contextOptions);
            _teamService = new TeamService(context, _mapper);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            _projectService = new ProjectService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now.AddDays(-5),
            };
                        
            var createdUser = await _userService.CreateUser(user);
            
            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };
            await _teamService.CreateTeam(team);

            var project1 = new ProjectCreateDTO()
            {
                Name = "FirstProjectName",
                Description = "FirstProjectsDescription",
                AuthorId = createdUser.Id,
                TeamId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };
            
            var project2 = new ProjectCreateDTO()
            {
                Name = "SecondProjectName",
                Description = "SecondProjectsDescription",
                AuthorId = createdUser.Id,
                TeamId = 1,
                CreatedAt = DateTime.Now.AddDays(-1),
                Deadline = DateTime.Now.AddMonths(5)
            };
            
            var createdProject1 = await _projectService.CreateProject(project1);
            var createdProject2 = await _projectService.CreateProject(project2);
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject1.Id,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription", 
                ProjectId = createdProject2.Id,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
                FinishedAt = DateTime.Now.AddDays(-1)
            };
            
            var task3 = new TaskCreateDTO() 
            { 
                Name = "TaskName",
                Description = "TaskDescription", 
                ProjectId = createdProject2.Id,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };

            await _taskService.CreateTask(task1);
            var createdTask2 = await _taskService.CreateTask(task2);
            await _taskService.CreateTask(task3);

            _linqService = new LinqQueriesService(context, _mapper);

            var queryResult = await _linqService.Query6(createdUser.Id);
            
            Assert.Equal(createdUser.Id, queryResult.User.Id);
            Assert.Equal(createdProject1.Id, queryResult.LastProject.Id);
            Assert.Equal(0, queryResult.IncompleteOrCancelledCount);
            Assert.Equal(queryResult.LongestTaskByDate.Id, createdTask2.Id);
        }

        [Fact]
        public async Task Query7_GetExpectedStructure()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _teamService = new TeamService(context, _mapper);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            _projectService = new ProjectService(context, _mapper);

            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };
            var createdTeam = await _teamService.CreateTeam(team);
            
            var user1 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now.AddDays(-5),
            };
            
            var user2 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "bogdanliusik@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now.AddDays(-5),
            };
                        
            var createdUser1 = await _userService.CreateUser(user1);
            var createdUser2 = await _userService.CreateUser(user2);

            var project = new ProjectCreateDTO()
            {
                Name = "FirstProjectName",
                Description = "FirstProjectsDescription",
                AuthorId = createdUser1.Id,
                TeamId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };

            var createdProject = await _projectService.CreateProject(project);

            var projectShortestTaskByName = new TaskCreateDTO() 
            { 
                Name = "Task",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = createdUser2.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var projectLongestTaskByDescription = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "Task Description Looooooongest", 
                ProjectId = createdProject.Id,
                PerformerId = createdUser1.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
                FinishedAt = DateTime.Now.AddDays(-1)
            };
            
            var task1 = new TaskCreateDTO() 
            { 
                Name = "Nameaaaaa",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = createdUser2.Id,
                CreatedAt = DateTime.Now.AddMonths(-1),
                FinishedAt = DateTime.Now
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription", 
                ProjectId = createdProject.Id,
                PerformerId = createdUser1.Id,
                CreatedAt = DateTime.Now.AddYears(-1),
                FinishedAt = DateTime.Now.AddDays(-1)
            };

            var createdProjectShortestTaskByName = await _taskService.CreateTask(projectShortestTaskByName);
            var createdProjectLongestTaskByDescription = await _taskService.CreateTask(projectLongestTaskByDescription);
            await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);
            
            _linqService = new LinqQueriesService(context, _mapper);

            var queryResult = await _linqService.Query7();
            var result = queryResult.ToList()[0];
            
            Assert.Equal(createdProject.Id, result.Project.Id);
            Assert.Equal(createdProjectShortestTaskByName.Id, result.ShortestTaskByName.Id);
            Assert.Equal(createdProjectLongestTaskByDescription.Id, result.LongestTaskByDescription.Id);
            Assert.Equal(2, result.TeamMembersCountByCondition);
        }
    }
}
﻿using System;

namespace ConsoleClient.Common.Models.User
{
    public class UserCreateModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int TeamId { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
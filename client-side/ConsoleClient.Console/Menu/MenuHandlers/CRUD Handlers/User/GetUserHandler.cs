﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.User;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User
{
    using System;
    public class GetUserHandler : BaseHandler
    {
        public GetUserHandler(CrudService crudService) : base(crudService) { }

        public override async Task Handle()
        {
            try
            {
                WriteSuccessLine("Enter user id: ");
                int id = Int32.Parse(Console.ReadLine());
                
                var user = await (Service as CrudService).GetEntityByIdAsync<UserModel>("users", id);
                
                WriteColorLine($"User ID: {user.Id}", ConsoleColor.Yellow);
                WriteColorLine($"User first name: {user.FirstName}", ConsoleColor.Yellow);
                WriteColorLine($"User last name: {user.LastName}", ConsoleColor.Yellow);
                WriteColorLine($"User email: {user.Email}", ConsoleColor.Yellow);
                WriteColorLine($"User team ID: {user.TeamId}", ConsoleColor.Yellow);
                WriteColorLine($"User birthday: {user.Birthday}", ConsoleColor.Yellow);
                WriteColorLine($"User registered at: {user.CreatedAt}", ConsoleColor.Yellow);
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
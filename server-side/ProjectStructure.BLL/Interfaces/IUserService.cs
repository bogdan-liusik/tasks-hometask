﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        Task<ICollection<UserDTO>> GetAllUsers();
        Task<UserDTO> GetUserById(int projectId);
        Task<ICollection<TaskDTO>> GetAllUnfinishedTasks(int id);
        Task<UserDTO> CreateUser(UserCreateDTO project);
        Task<UserDTO> UpdateUser(UserUpdateDTO project);
        Task DeleteUser(int projectId);
    }
}
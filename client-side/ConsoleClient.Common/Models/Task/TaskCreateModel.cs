﻿using System;
using ConsoleClient.Common.Models.Task.Enums;

namespace ConsoleClient.Common.Models.Task
{
    public class TaskCreateModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime FinishedAt { get; set; }
    }
}
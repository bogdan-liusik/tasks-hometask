﻿using System;

namespace ConsoleClient.Console._Menu
{
    public static class MenuHelper
    {
        public static void WriteColorLine(string @string, ConsoleColor color)
        {
            PrintColorLine(@string, color);
        }

        public static void WriteSuccessLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Green);
        }

        public static void WriteErrorLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Red);
        }

        public static void WriteEnterAnyKeyToContinue()
        {
            System.Console.WriteLine("Please, enter any key to continue...");
            System.Console.ReadKey();
        }
        
        public static void ShowCommands()
        {
             WriteColorLine("┌" + new string('─', 13) + "CRUD SERVICE" + new string('─', 11) + "┐", ConsoleColor.Green);
             System.Console.ForegroundColor = ConsoleColor.Cyan;
             System.Console.WriteLine("| 1. Create user.                    |");
             System.Console.WriteLine("| 2. Update user.                    |");
             System.Console.WriteLine("| 3. Get user by id.                 |");
             System.Console.WriteLine("| 4. Delete user.                    |");
             System.Console.WriteLine("| 5. Create project.                 |");
             System.Console.WriteLine("| 6. Update project.                 |");
             System.Console.WriteLine("| 7. Get project by id.              |");
             System.Console.WriteLine("| 8. Delete project.                 |");
             System.Console.WriteLine("| 9. Create task.                    |");
             System.Console.WriteLine("| 10. Update task.                   |");
             System.Console.WriteLine("| 11. Get task by id.                |");
             System.Console.WriteLine("| 12. Delete task.                   |");
             System.Console.WriteLine("| 13. Create team.                   |");
             System.Console.WriteLine("| 14. Update team.                   |");
             System.Console.WriteLine("| 15. Get team by id.                |");
             System.Console.WriteLine("| 16. Delete team.                   |");
             System.Console.WriteLine("| 17. Result of Query 1.             |");
             System.Console.WriteLine("| 18. Result of Query 2.             |");
             System.Console.WriteLine("| 19. Result of Query 3.             |");
             System.Console.WriteLine("| 20. Result of Query 4.             |");
             System.Console.WriteLine("| 21. Result of Query 5.             |");
             System.Console.WriteLine("| 22. Result of Query 6.             |");
             System.Console.WriteLine("| 23. Result of Query 7.             |");
             WriteColorLine("| 0. Exit                            |", ConsoleColor.Red);
             WriteColorLine("└" + new string('─', 36) + "┘", ConsoleColor.Green);
         }
        
        private static void PrintColorLine(string @string, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            System.Console.WriteLine(@string);
            System.Console.ResetColor();
        }
    }
}
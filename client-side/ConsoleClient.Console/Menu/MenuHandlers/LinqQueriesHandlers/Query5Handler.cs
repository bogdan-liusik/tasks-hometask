﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers
{
    using System;
    public class Query5Handler : BaseHandler
    {
        public Query5Handler(LinqQueriesService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list of users alphabetically by first_name (ascending) with sorted tasks by name length (descending).", ConsoleColor.Yellow);

                var results = await (Service as LinqQueriesService).Query5();
                
                if (results.Count == 0)
                {
                    Console.WriteLine("List is empty.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var result in results)
                {
                    WriteSuccessLine($"User ID: {result.UserModel.Id} | First name {result.UserModel.FirstName}");
                    WriteColorLine("User sorted tasks by name length:", ConsoleColor.Yellow);
                    foreach (var task in result.Tasks)
                    {
                        Console.WriteLine($"{task.Name}");
                    }
                }
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query5StructureDTO
    {
        public UserDTO User { get; set; }
        public ICollection<TaskDTO> Tasks { get; set; }
    }
}
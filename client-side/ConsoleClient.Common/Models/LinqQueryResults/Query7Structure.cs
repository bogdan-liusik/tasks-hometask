﻿using ConsoleClient.Common.Models.Task;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query7Structure
    {
        public Project.ProjectModel ProjectModel { get; set; }
        public TaskModel LongestTaskModelByDescription { get; set; }
        public TaskModel ShortestTaskModelByName { get; set; }
        public int TeamMembersCountByCondition { get; set; }
    }
}
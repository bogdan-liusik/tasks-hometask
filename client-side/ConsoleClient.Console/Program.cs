﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu;

namespace ConsoleClient.Console
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            const string baseAddress = "https://localhost:5001/api/";
            using var httpClientService = new HttpClientService(baseAddress);
            
            await new Menu(httpClientService).Start();
        }
    }
}
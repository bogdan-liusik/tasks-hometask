﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.Task.Enums;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Tasks
{
    using System;
    public class UpdateTaskHandler : BaseHandler
    {
        public UpdateTaskHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                var taskCreate = new TaskUpdateModel();
                WriteColorLine("Enter task id: ", ConsoleColor.Yellow);
                taskCreate.Id = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter new task name: ", ConsoleColor.Yellow);
                taskCreate.Name = Console.ReadLine();
                WriteColorLine("Enter new task description: ", ConsoleColor.Yellow);
                taskCreate.Description = Console.ReadLine();
                WriteColorLine("Enter new task state (0-3): ", ConsoleColor.Yellow);
                taskCreate.State = Enum.Parse<TaskState>(Console.ReadLine());
                
                await (Service as CrudService).UpdateEntityAsync<TaskUpdateModel, TaskModel>(taskCreate, "tasks");
                
                WriteSuccessLine("Task successfully updated!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteColorLine(ex.Message, ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
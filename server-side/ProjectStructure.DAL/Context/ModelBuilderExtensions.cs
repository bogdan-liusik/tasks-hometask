﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context.ModelConfigurations;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void ConfigureRelations(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public static void ConfigureModels(this ModelBuilder modelBuilder)
        {
            new ProjectEntityTypeConfiguration().Configure(modelBuilder.Entity<Project>());
            new UserEntityTypeConfiguration().Configure(modelBuilder.Entity<User>());
            new TaskEntityTypeConfiguration().Configure(modelBuilder.Entity<Task>());
            new TeamEntityTypeConfiguration().Configure(modelBuilder.Entity<Team>());
        }
        
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var dataGenerator = new RandomDataGenerator();
            var teams = dataGenerator.GenerateRandomTeams();
            var users = dataGenerator.GenerateRandomUsers(teams);
            var projects = dataGenerator.GenerateRandomProjects(users, teams);
            var tasks = dataGenerator.GenerateRandomTasks(users, projects);
                 
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
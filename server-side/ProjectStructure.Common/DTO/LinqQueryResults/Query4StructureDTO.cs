﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.Common.DTO.LinqQueryResults
{
    public class Query4StructureDTO
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public ICollection<UserDTO> Members { get; set; }
    }
}
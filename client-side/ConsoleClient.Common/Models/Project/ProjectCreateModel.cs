﻿using System;

namespace ConsoleClient.Common.Models.Project
{
    public class ProjectCreateModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime Deadline { get; set; }
    }
}
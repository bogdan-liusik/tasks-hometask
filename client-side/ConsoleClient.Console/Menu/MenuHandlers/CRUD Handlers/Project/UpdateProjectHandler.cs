﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Project
{
    using System;
    public class UpdateProjectHandler : BaseHandler
    {
        public UpdateProjectHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                var projectUpdate = new ProjectUpdateModel();
                WriteColorLine("Enter project id: ", ConsoleColor.Yellow);
                projectUpdate.Id = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter project name: ", ConsoleColor.Yellow);
                projectUpdate.Name = Console.ReadLine();
                WriteColorLine("Enter project description: ", ConsoleColor.Yellow);
                projectUpdate.Description = Console.ReadLine();
                
                await (Service as CrudService).UpdateEntityAsync<ProjectUpdateModel, ProjectModel>(projectUpdate, "projects");
                
                WriteSuccessLine("Project successfully updated!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteColorLine(ex.Message, ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
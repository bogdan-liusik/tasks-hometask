﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Project
{
    using System;
    public class GetProjectHandler : BaseHandler
    {
        public GetProjectHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                WriteSuccessLine("Enter project id: ");
                int id = Int32.Parse(Console.ReadLine());
                
                var project = await (Service as CrudService).GetEntityByIdAsync<ProjectModel>("projects", id);
                
                WriteColorLine($"Project ID: {project.Id}", ConsoleColor.Yellow);
                WriteColorLine($"Project name: {project.Id}", ConsoleColor.Yellow);
                WriteColorLine($"Project description: {project.Description}", ConsoleColor.Yellow);
                WriteColorLine($"Project author ID: {project.AuthorId}", ConsoleColor.Yellow);
                WriteColorLine($"Project team Id: {project.TeamId}", ConsoleColor.Yellow);
                WriteColorLine($"Project deadline: {project.Deadline}", ConsoleColor.Yellow);
                WriteColorLine($"Project created at: {project.CreatedAt}", ConsoleColor.Yellow);
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
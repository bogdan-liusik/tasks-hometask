﻿using System;
using System.Collections.Generic;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public ICollection<Task> Tasks { get; set; }
    }
}
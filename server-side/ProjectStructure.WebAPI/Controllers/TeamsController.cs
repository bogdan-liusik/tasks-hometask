﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        
        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> GetAllTeams()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> GetTeamById(int id)
        {
            return Ok(await _teamService.GetTeamById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] TeamCreateDTO teamCreateDto)
        {
            return Created("", await _teamService.CreateTeam(teamCreateDto));
        }
        
        [HttpPut]
        public async Task<ActionResult<TeamDTO>> UpdateTeam([FromBody] TeamUpdateDTO teamUpdateDto)
        {
            return Ok(await _teamService.UpdateTeam(teamUpdateDto));
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            await _teamService.DeleteTeam(id);
            return NoContent();
        }
    }
}
﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Project
{
    using System;
    public class CreateProjectHandler : BaseHandler
    {
        public CreateProjectHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                var projectCreate = new ProjectCreateModel();
                WriteColorLine("Enter project name: ", ConsoleColor.Yellow);
                projectCreate.Name = Console.ReadLine();
                WriteColorLine("Enter project description: ", ConsoleColor.Yellow);
                projectCreate.Description = Console.ReadLine();
                WriteColorLine("Enter author ID: ", ConsoleColor.Yellow);
                projectCreate.AuthorId = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter team ID: ", ConsoleColor.Yellow);
                projectCreate.TeamId = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter deadline (in format dd/mm/yyyy): ", ConsoleColor.Yellow);
                projectCreate.Deadline = DateTime.Parse(Console.ReadLine());
                
                var createdProject = await (Service as CrudService).CreateEntityAsync<ProjectCreateModel, ProjectModel>(projectCreate, "projects");
                
                WriteSuccessLine($"Project successfully created with id {createdProject.Id}!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteColorLine(ex.Message, ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
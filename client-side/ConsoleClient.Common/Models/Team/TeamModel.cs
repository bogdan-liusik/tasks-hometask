﻿using System;

namespace ConsoleClient.Common.Models.Team
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
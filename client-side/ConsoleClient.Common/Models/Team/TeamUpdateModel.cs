﻿namespace ConsoleClient.Common.Models.Team
{
    public class TeamUpdateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
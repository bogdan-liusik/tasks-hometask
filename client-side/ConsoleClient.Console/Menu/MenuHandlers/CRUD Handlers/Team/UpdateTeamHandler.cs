﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Team;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Team
{
    using System;
    public class UpdateTeamHandler : BaseHandler
    {
        public UpdateTeamHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                var teamCreate = new TeamUpdateModel();
                WriteColorLine("Enter team id: ", ConsoleColor.Yellow);
                teamCreate.Id = Int32.Parse(Console.ReadLine());
                WriteColorLine("Enter new team name: ", ConsoleColor.Yellow);
                teamCreate.Name = Console.ReadLine();
                
                await (Service as CrudService).UpdateEntityAsync<TeamUpdateModel, TeamModel>(teamCreate, "teams");
                
                WriteSuccessLine("Team successfully updated!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteColorLine(ex.Message, ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
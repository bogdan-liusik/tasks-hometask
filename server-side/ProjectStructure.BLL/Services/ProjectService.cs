﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<ProjectDTO>> GetAllProjects()
        {
            var projects = await GetAllEntitiesAsync<Project>();
            return await Task.WhenAll(projects
                .Select(async project => await Task.Run(() => _mapper.Map<ProjectDTO>(project)))
                .ToList());
        }
        
        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var projectEntity = await GetEntityByIdAsync<Project>(id);
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
        
        public async Task<ProjectDTO> CreateProject(ProjectCreateDTO projectCreateDto)
        {
            // if (!await _context.Users.AnyAsync(u => u.Id == projectCreateDto.AuthorId))
            // {
            //     throw new NotFoundException($"AuthorId {projectCreateDto.AuthorId}. No users with such id.");
            // }
            //
            // if (!await _context.Teams.AnyAsync(t => t.Id == projectCreateDto.TeamId))
            // {
            //     throw new NotFoundException($"TeamId {projectCreateDto.AuthorId}. No team with such id.");
            // }
            
            var projectEntity = _mapper.Map<Project>(projectCreateDto);

            var createdProject = await _context.Projects.AddAsync(projectEntity);
            
            await _context.SaveChangesAsync();
            
            return _mapper.Map<ProjectDTO>(createdProject.Entity);
        }

        public async Task<ProjectDTO> UpdateProject(ProjectUpdateDTO projectUpdateDto)
        {
            var projectEntity = await GetEntityByIdAsync<Project>(projectUpdateDto.Id);

            projectEntity.Name = projectUpdateDto.Name;
            projectEntity.Description = projectUpdateDto.Description;
            
            await _context.SaveChangesAsync();
            
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
        
        public async Task DeleteProject(int id)
        {
            var entityProject = await GetEntityByIdAsync<Project>(id);
            _context.Projects.Remove(entityProject);
            await _context.SaveChangesAsync();
        }
    }
}
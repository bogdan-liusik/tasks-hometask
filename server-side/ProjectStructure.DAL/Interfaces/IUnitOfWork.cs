﻿using System.Threading.Tasks;
using ProjectStructure.DAL.Entities.Abstract;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity: BaseEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
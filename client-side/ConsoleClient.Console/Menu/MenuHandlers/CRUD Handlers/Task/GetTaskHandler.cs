﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Task;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Tasks
{
    using System;
    public class GetTaskHandler : BaseHandler
    {
        public GetTaskHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                WriteSuccessLine("Enter task id: ");
                int id = Int32.Parse(Console.ReadLine());
                
                var task = await (Service as CrudService).GetEntityByIdAsync<TaskModel>("tasks", id);
                
                WriteColorLine($"Task ID: {task.Id}", ConsoleColor.Yellow);
                WriteColorLine($"Task name: {task.Id}", ConsoleColor.Yellow);
                WriteColorLine($"Task description: {task.Description}", ConsoleColor.Yellow);
                WriteColorLine($"Task state: {task.State}", ConsoleColor.Yellow);
                WriteColorLine($"Task project ID: {task.ProjectId}", ConsoleColor.Yellow);
                WriteColorLine($"Task performer ID: {task.PerformerId}", ConsoleColor.Yellow);
                WriteColorLine($"Task created at: {task.CreatedAt}", ConsoleColor.Yellow);
                WriteColorLine($"Task finished at: {task.FinishedAt}", ConsoleColor.Yellow);
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
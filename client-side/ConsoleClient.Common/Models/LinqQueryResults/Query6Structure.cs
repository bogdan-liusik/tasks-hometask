﻿using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Common.Models.User;

namespace ConsoleClient.Common.Models.LinqQueryResults
{
    public class Query6Structure
    {
        public UserModel UserModel { get; set; }
        public ProjectModel LastProjectModel { get; set; }
        public int NumberOfTasksInLastProject { get; set; }
        public int IncompleteOrCancelledCount { get; set; }
        public TaskModel LongestTaskModelByDate { get; set; }
    }
}
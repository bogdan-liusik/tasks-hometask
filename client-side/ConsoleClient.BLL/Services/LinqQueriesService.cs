﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsoleClient.BLL.Services.Abstract;
using ConsoleClient.Common.Models.LinqQueryResults;
using ConsoleClient.Common.Models.Project;
using ConsoleClient.Common.Models.Task;

namespace ConsoleClient.BLL.Services
{
    public class LinqQueriesService : BaseService
    {
        private const string ApiAddress = "linqqueries";

        public LinqQueriesService(HttpClientService httpClientService) : base(httpClientService) { }

        public async Task<Dictionary<ProjectModel, int>> Query1(int id)
        {
            var result = await _httpClientService.FetchListObjectsAsync<Query1Structure>($"{ApiAddress}/query1/{id}");
            return result.ToDictionary(r => r.ProjectModel, r => r.TasksCount);
        }
            
        public async Task<List<TaskModel>> Query2(int id)
        {
            return await _httpClientService.FetchListObjectsAsync<TaskModel>($"{ApiAddress}/query2/{id}");
        }

        public async Task<List<Query3Structure>> Query3(int id)
        {
            return await _httpClientService.FetchListObjectsAsync<Query3Structure>($"{ApiAddress}/query3/{id}");
        }

        public async Task<List<Query4Structure>> Query4()
        {
            return await _httpClientService.FetchListObjectsAsync<Query4Structure>($"{ApiAddress}/query4");
        }

        public async Task<List<Query5Structure>> Query5()
        {
            return await _httpClientService.FetchListObjectsAsync<Query5Structure>($"{ApiAddress}/query5");
        }

        public async Task<Query6Structure> Query6(int id)
        {
            return await _httpClientService.FetchObjectAsync<Query6Structure>($"{ApiAddress}/query6/{id}");
        }

        public async Task<List<Query7Structure>> Query7()
        {
            return await _httpClientService.FetchListObjectsAsync<Query7Structure>($"{ApiAddress}/query7");
        }
    }
}
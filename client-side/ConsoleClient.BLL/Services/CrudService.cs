﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleClient.BLL.Services.Abstract;

namespace ConsoleClient.BLL.Services
{
    public class CrudService : BaseService
    {

        public CrudService(HttpClientService httpClientService) : base(httpClientService) { }

        public async Task<ICollection<TEntity>> GetAllEntitiesAsync<TEntity>(string endpoint)
        {
            return await _httpClientService.FetchListObjectsAsync<TEntity>(endpoint);
        }
            
        public async Task<TEntity> GetEntityByIdAsync<TEntity>(string endpoint, int id)
        {
            return await _httpClientService.FetchObjectAsync<TEntity>($"{endpoint}/{id}");
        }

        public async Task<TResult> CreateEntityAsync<TEntity, TResult>(TEntity entity, string endpoint)
        {
            return await _httpClientService.PostAsync<TEntity, TResult>(entity, endpoint);
        }
        
        public async Task<TResult> UpdateEntityAsync<TEntity, TResult>(TEntity entity, string endpoint)
        {
            return await _httpClientService.PutAsync<TEntity, TResult>(entity, endpoint);
        }

        public async Task DeleteEntityAsync(int id, string endpoint)
        {
            await _httpClientService.DeleteAsync(id, endpoint);
        }
    }
}
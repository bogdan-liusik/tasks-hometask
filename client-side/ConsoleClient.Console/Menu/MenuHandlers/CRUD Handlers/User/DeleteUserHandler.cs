﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User
{
    using System;
    public class DeleteUserHandler : BaseHandler
    {
        public DeleteUserHandler(CrudService crudService) : base(crudService) { }

        public override async Task Handle()
        {
            try
            {
                WriteSuccessLine("Enter user id: ");
                int id = Int32.Parse(Console.ReadLine());

                await (Service as CrudService).DeleteEntityAsync(id, "users");
                
                WriteSuccessLine("User successfully deleted!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
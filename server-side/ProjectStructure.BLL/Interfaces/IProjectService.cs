﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<ICollection<ProjectDTO>> GetAllProjects();
        Task<ProjectDTO> GetProjectById(int projectId);
        Task<ProjectDTO> CreateProject(ProjectCreateDTO project);
        Task<ProjectDTO> UpdateProject(ProjectUpdateDTO project);
        Task DeleteProject(int projectId);
    }
}
﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.LinqQueriesHandlers
{
    using System;
    public class Query1Handler : BaseHandler
    {
        public Query1Handler(LinqQueriesService service) : base(service) { }

        public override async Task Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the number of tasks of a particular user's project (by id) (dictionary, where the key is the project, and the value is the number of tasks).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id: ");
                string id = Console.ReadLine();
                
                var result = await (Service as LinqQueriesService).Query1(Int32.Parse(id));

                if (result.Count == 0)
                {
                    Console.WriteLine("This user has no projects of his own.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var (key, value) in result)
                {
                    WriteSuccessLine($"ProjectID: {key.Id} | ProjectName: {key.Name} | Number of tasks: {value}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
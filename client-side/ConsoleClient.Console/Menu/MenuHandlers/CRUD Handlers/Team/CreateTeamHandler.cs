﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Common.Models.Team;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using static ConsoleClient.Console._Menu.MenuHelper;

namespace ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.Team
{
    using System;
    public class CreateTeamHandler : BaseHandler
    {
        public CreateTeamHandler(CrudService service) : base(service) { }

        public override async Task Handle()
        { 
            try
            {
                var teamCreate = new TeamCreateModel();
                WriteColorLine("Enter team name: ", ConsoleColor.Yellow);
                teamCreate.Name = Console.ReadLine();

                var createdTeam = await (Service as CrudService).CreateEntityAsync<TeamCreateModel, TeamModel>(teamCreate, "teams");
                
                WriteSuccessLine($"Team successfully created with id {createdTeam.Id}!");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                WriteErrorLine("Something went wrong...");
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}
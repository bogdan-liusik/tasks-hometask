﻿using Xunit;
using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Fakes;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Enums;

namespace ProjectStructure.BLL.Tests.Tests
{
    public class UserServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _contextOptions;
        
        private UserService _userService;
        private TaskService _taskService;

        public UserServiceTests()
        {
            _contextOptions = new DbContextOptionsBuilder<ProjectsContext>()
                .UseInMemoryDatabase("TestProjectsDB_UserService")
                .Options;
            
            _mapper = new Mapper(new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<UserProfile>();
                configuration.AddProfile<TaskProfile>();
            }));
        }

        [Fact]
        public async Task CreateUser_ThenCheckProperties()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan0",
                LastName = "Liusik0",
                Email = "bogdanliusik0@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUser = await _userService.CreateUser(user);
            
            //as it's first user, ID must be 1
            Assert.Equal(1, createdUser.Id);
            Assert.Equal(createdUser.FirstName, user.FirstName);
            Assert.Equal(createdUser.LastName, user.LastName);
            Assert.Equal(createdUser.Email, user.Email);
            Assert.Equal(createdUser.BirthDay, user.BirthDay);
            Assert.Equal(createdUser.CreatedAt, user.CreatedAt);
        }
        
        [Fact]
        public async Task CreateUser_AddTwoUsers_ThenCheckTheirIDs()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            var user1 = new UserCreateDTO()
            {
                FirstName = "Bogdan1",
                LastName = "Liusik1",
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };
            
            var user2 = new UserCreateDTO()
            {
                FirstName = "Bogdan2",
                LastName = "Liusik2",
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2001"),
                CreatedAt = DateTime.Now,
            };
            
            var createdUser1 = await _userService.CreateUser(user1);
            var createdUser2 = await _userService.CreateUser(user2);
            
            Assert.Equal(1, createdUser1.Id);
            Assert.Equal(2, createdUser2.Id);
        }
        
        [Fact]
        public async Task GetAllUnfinishedTasks_WhenAddTwoUnfinishedTasks_ThenResult2()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };

            var createdUser = await _userService.CreateUser(user);

            var task1 = new TaskCreateDTO() 
            { 
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.ToDo,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-1)
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription",
                State = TaskState.InProgress,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddYears(-1)
            };
            
            await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);

            var result = await _userService.GetAllUnfinishedTasks(createdUser.Id);
            
            Assert.Equal(2, result.Count);
        }
        
        [Fact]
        public async Task GetAllUnfinishedTasks_WhenAddFinishedAndCancelledTasks_ThenResult0()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _userService = new UserService(context, _mapper);
            _taskService = new TaskService(context, _mapper);
            
            var user = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };

            var createdUser = await _userService.CreateUser(user);

            var task1 = new TaskCreateDTO() 
            { 
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.Done,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddMonths(-1)
            };
            
            var task2 = new TaskCreateDTO() 
            { 
                Name = "TaskName aaaaaaaaaa",
                Description = "TaskDescription",
                State = TaskState.Canceled,
                PerformerId = createdUser.Id,
                CreatedAt = DateTime.Now.AddYears(-1)
            };
            
            await _taskService.CreateTask(task1);
            await _taskService.CreateTask(task2);
            
            Assert.Empty(await _userService.GetAllUnfinishedTasks(createdUser.Id));
        }
    }
}
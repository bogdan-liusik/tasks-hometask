﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using ConsoleClient.Common.Models.Task;
using ConsoleClient.Common.Models.Task.Enums;

namespace ConsoleClient.BLL.Services
{
    public class DelayedTaskService
    {
        private readonly HttpClientService _httpClientService;
        private readonly Timer _timer;
        private readonly Random _random;
        
        public DelayedTaskService(HttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
            _timer = new Timer();
            _random = new Random();
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var completionSource = new TaskCompletionSource<int>();

            _timer.Elapsed += async (sender, args) =>
            {
                try
                {
                    var allTasks = await _httpClientService.FetchListObjectsAsync<TaskModel>("tasks");
                
                    var suitableTasks = allTasks
                        .Where(t => t.State != TaskState.Canceled && t.State != TaskState.Done)
                        .ToList();
                
                    var randomTask = suitableTasks[_random.Next(suitableTasks.Count)];
                
                    await _httpClientService.PutAsync<TaskUpdateModel, TaskModel>(new TaskUpdateModel()
                    {
                        Id = randomTask.Id,
                        Name = randomTask.Name,
                        Description = randomTask.Description,
                        State = TaskState.Done
                    }, "tasks");
                    
                    completionSource.TrySetResult(randomTask.Id);
                }
                catch (Exception ex)
                {
                    completionSource.SetException(ex);
                }
            };
            
            _timer.Interval = delay;
            _timer.Enabled = true;
            _timer.AutoReset = false;
            
            return await completionSource.Task;
        }
    }
}